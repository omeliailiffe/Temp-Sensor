#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino

#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>         //https://github.com/tzapu/WiFiManager
#include <Wire.h>
#include "SparkFunHTU21D.h"
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"
#include <FastLED.h>
#include <Ticker.h>

#define AIO_SERVER      "io.adafruit.com"
#define AIO_SERVERPORT  1883
#define AIO_USERNAME  "harryiliffe"
#define AIO_KEY       "32915ea1424244ecb5bd0e387b622d6c"

#define NUM_LEDS 2
#define DATA_PIN 14
Ticker ledblink;
bool ledon = false;
CRGB leds[NUM_LEDS];

/************ Global State (you don't need to change this!) ******************/

// Create an ESP8266 WiFiClient class to connect to the MQTT server.
WiFiClient client;

// Setup the MQTT client class by passing in the WiFi client and MQTT server and login details.
Adafruit_MQTT_Client mqtt(&client, AIO_SERVER, AIO_SERVERPORT, AIO_USERNAME, AIO_KEY);

WiFiManager wifiManager;

/****************************** Feeds ***************************************/

Adafruit_MQTT_Publish tempIO = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/showhome.temp001");
Adafruit_MQTT_Publish humIO = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/showhome.humidity001");

HTU21D dht;

CRGB colors[3] = {CRGB::Red, CRGB::Orange, CRGB::Green};

void fillLED(CRGB color) {
  ledblink.detach();
  fill_solid(leds, NUM_LEDS, color);
  FastLED.show();
}

void blinkLED(int color) {
  if (ledon) {
    fill_solid(leds, NUM_LEDS, colors[color]);
    ledon = !ledon;
  } else {
    fill_solid(leds, NUM_LEDS, CRGB::Black);
    ledon = !ledon;
  }
  FastLED.show();
}

void setup() {
  Serial.begin(115200);
  FastLED.addLeds<WS2812B, DATA_PIN, GRB>(leds, NUM_LEDS);

  ledblink.attach(0.5, blinkLED, 1);
  //  wifiManager.resetSettings();

  wifiManager.autoConnect("TemperatureSetupWifi");
  Serial.println("Wifi Connected");
  dht.begin();

  fillLED(CRGB::Green);

  float humidity = dht.readHumidity();
  float temperature = dht.readTemperature();
  WiFi.mode(WIFI_STA);
  wifi_set_sleep_type(LIGHT_SLEEP_T);
  delay(1000);

}

void loop() {
  MQTT_connect();

  float humidity = dht.readHumidity();
  float temperature = dht.readTemperature();

  Serial.print("Temperature: "); Serial.print(temperature);
  Serial.print("\t\tHumidity: "); Serial.println(humidity);


  if (temperature != 998 || humidity != 998) {
    fillLED(CRGB::Green);
    if (! tempIO.publish(temperature++) || ! humIO.publish(humidity++)) { //wifi connection gone
      Serial.println(F("Upload Failed"));
      fillLED(CRGB::Orange);
      delay(5000);
      wifiManager.resetSettings();
      ESP.restart();
    }
  } else {
    ledblink.attach(0.3, blinkLED, 0); //Sensor Disconected
  }
  fillLED(CRGB::Black);

    delay(60000);
//  delay(2000);
}

void MQTT_connect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return;
  }

  Serial.print("Connecting to MQTT... ");

  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
    fillLED(CRGB::Orange);

    Serial.println(mqtt.connectErrorString(ret));
    Serial.println("Retrying MQTT connection in 5 seconds...");
    mqtt.disconnect();
    delay(5000);  // wait 5 seconds
    retries--;
    if (retries == 0) {
      // basically die and wait for WDT to reset me
      wifiManager.resetSettings();
      ESP.restart();
    }
  }
  Serial.println("MQTT Connected!");
}
